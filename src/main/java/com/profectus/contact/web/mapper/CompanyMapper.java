package com.profectus.contact.web.mapper;

import com.profectus.contact.domain.Company;
import com.profectus.contact.dto.CompanyDTO;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = ContactMapper.class)
public interface CompanyMapper {

    Company toBusiness(CompanyDTO dto);

    CompanyDTO toDTO(Company company);

    default List<CompanyDTO> toListDTO(List<Company> companies) {
        return companies != null ? companies.stream().map(company -> this.toDTO(company)).collect(Collectors.toList()) : null;
    }

}
