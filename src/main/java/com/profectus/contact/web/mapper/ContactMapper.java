package com.profectus.contact.web.mapper;

import com.profectus.contact.domain.Contact;
import com.profectus.contact.dto.ContactDTO;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ContactMapper {


    Contact toBusiness(ContactDTO dto);

    ContactDTO toDTO(Contact contact);

    default List<ContactDTO> toListDTO(List<Contact> contacts) {
        return contacts != null ? contacts.stream().map(contact -> this.toDTO(contact)).collect(Collectors.toList()) : null;
    }

}
