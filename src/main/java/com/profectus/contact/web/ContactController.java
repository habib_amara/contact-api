package com.profectus.contact.web;

import com.profectus.contact.dto.ContactDTO;
import com.profectus.contact.service.ContactService;
import com.profectus.contact.web.mapper.ContactMapper;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/contacts")
public class ContactController {

    private final ContactService contactService;
    private final ContactMapper contactMapper;

    @PostMapping
    public ResponseEntity<ContactDTO> create(@RequestBody ContactDTO contact) {
        return ResponseEntity.ok(contactMapper.toDTO(
                contactService.create(contactMapper.toBusiness(contact)))
        );
    }

    @PutMapping("/{id}")
    public ResponseEntity<ContactDTO> update(@NotNull @RequestBody ContactDTO contact,
                                             @PathVariable Long id) {
        if (contact.getId() != id) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(contactMapper.toDTO(
                contactService.update(contactMapper.toBusiness(contact)))
        );

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable Long id) {
        return ResponseEntity.ok(contactService.delete(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ContactDTO> getContact(@PathVariable Long id) {
        return ResponseEntity.ok(contactMapper.toDTO(
                contactService.getContact(id))
        );
    }

    @GetMapping
    public ResponseEntity<List<ContactDTO>> getContacts() {
        return ResponseEntity.ok(contactMapper.toListDTO(
                contactService.getContacts())
        );
    }
}
