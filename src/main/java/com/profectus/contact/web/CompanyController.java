package com.profectus.contact.web;

import com.profectus.contact.dto.CompanyDTO;
import com.profectus.contact.service.CompanyService;
import com.profectus.contact.web.mapper.CompanyMapper;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/companies")
public class CompanyController {

    private final CompanyService companyService;
    private final CompanyMapper companyMapper;

    @PostMapping
    public ResponseEntity<CompanyDTO> create(@RequestBody CompanyDTO company) {
        return ResponseEntity.ok(companyMapper.toDTO(
                companyService.create(companyMapper.toBusiness(company)))
        );
    }

    @PutMapping("/{id}")
    public ResponseEntity<CompanyDTO> update(@NotNull @RequestBody CompanyDTO contact,
                                             @PathVariable Long id) {
        if (contact.getId() != id) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(companyMapper.toDTO(
                companyService.update(companyMapper.toBusiness(contact)))
        );
    }

    @GetMapping("/{companyId}/addcontact/{contactId}")
    public ResponseEntity<CompanyDTO> addContactToCompany( @PathVariable Long companyId,
                                                           @PathVariable Long contactId) {
        return ResponseEntity.ok(companyMapper.toDTO(
                companyService.addContactToCompany(companyId, contactId))
        );
    }

    @GetMapping("/{tvaNumber}")
    public ResponseEntity<CompanyDTO> getCompany(@PathVariable Integer tvaNumber) {
        return ResponseEntity.ok(companyMapper.toDTO(
                companyService.getByTVANumber(tvaNumber))
        );
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable Long id) {
        return ResponseEntity.ok(companyService.delete(id));
    }

    @GetMapping
    public ResponseEntity<List<CompanyDTO>> getCompanies() {
        return ResponseEntity.ok(companyMapper.toListDTO(
                companyService.getCompanies())
        );
    }
}
