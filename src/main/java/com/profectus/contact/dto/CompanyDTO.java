package com.profectus.contact.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class CompanyDTO {

    private Long id;
    private String address;
    private Integer tvaNumber;

    private Set<ContactDTO> contacts;
}
