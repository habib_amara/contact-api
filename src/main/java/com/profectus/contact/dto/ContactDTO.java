package com.profectus.contact.dto;

import com.profectus.contact.domain.ContactType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ContactDTO {

    private Long id;
    private String firstname;
    private String lastname;
    private String address;
    private Integer tvaNumber;
    private ContactType type;

}
