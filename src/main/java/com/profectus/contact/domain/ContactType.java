package com.profectus.contact.domain;

public enum ContactType {
    FREELANCE, EMPLOYEE
}
