package com.profectus.contact.domain;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "COMPANY")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "company_generator")
    @SequenceGenerator(name = "company_generator", sequenceName = "company_seq")
    private Long id;

    private String address;
    private Integer tvaNumber;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "companies")
    private Set<Contact> contacts = new HashSet<>();

    public void update(Company company) {
        if (company != null) {
            this.address = company.getAddress();
            this.tvaNumber = company.getTvaNumber();
            this.contacts = company.getContacts();
        }
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }
}
