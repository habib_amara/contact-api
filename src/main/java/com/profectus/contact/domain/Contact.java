package com.profectus.contact.domain;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "CONTACT")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_generator")
    @SequenceGenerator(name = "contact_generator", sequenceName = "contact_seq")
    private Long id;

    private String firstname;
    private String lastname;
    private String address;
    private Integer tvaNumber;

    @Enumerated(EnumType.STRING)
    private ContactType type;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "contact_companies",
            joinColumns = {@JoinColumn(name = "contact_id")},
            inverseJoinColumns = {@JoinColumn(name = "company_id")
            })
    private Set<Company> companies = new HashSet<>();

    public void update(Contact contact) {
        if (contact != null) {
            this.firstname = contact.getFirstname();
            this.lastname = contact.getLastname();
            this.address = contact.getAddress();
            this.tvaNumber = contact.getTvaNumber();
            this.type = contact.getType();
            this.companies = contact.getCompanies();
        }
    }

    public void addCompany(Company company) {
        this.companies.add(company);
    }

}
