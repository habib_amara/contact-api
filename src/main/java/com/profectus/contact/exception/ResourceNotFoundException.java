package com.profectus.contact.exception;

public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(Long id) {
        super(String.format("Resource with Id %d not found", id));
    }
}
