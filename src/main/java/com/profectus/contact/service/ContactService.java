package com.profectus.contact.service;

import com.profectus.contact.domain.Contact;

import java.util.List;

public interface ContactService {

    Contact create(Contact contact);

    Contact update(Contact contact);

    Long delete(Long id);

    Contact getContact(Long id);

    List<Contact> getContacts();

}
