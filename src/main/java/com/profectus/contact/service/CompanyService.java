package com.profectus.contact.service;

import com.profectus.contact.domain.Company;

import java.util.List;

public interface CompanyService {

    Company create(Company contact);

    Company update(Company contact);

    Company addContactToCompany(Long companyId, Long contactId);

    Long delete(Long id);

    Company getByTVANumber(Integer tvaNumber);

    List<Company> getCompanies();

}
