package com.profectus.contact.service.impl;

import com.profectus.contact.domain.Company;
import com.profectus.contact.domain.Contact;
import com.profectus.contact.exception.ResourceNotFoundException;
import com.profectus.contact.repository.CompanyRepository;
import com.profectus.contact.repository.ContactRepository;
import com.profectus.contact.service.CompanyService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;

    private final ContactRepository contactRepository;

    @Override
    public Company create(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public Company update(Company companyToUpdate) {
        if (companyToUpdate != null) {
            Company company = companyRepository.findById(companyToUpdate.getId())
                    .orElseThrow(() -> new ResourceNotFoundException(companyToUpdate.getId()));
            company.update(companyToUpdate);
            return companyRepository.save(company);
        }
        return null;
    }

    @Override
    public Company addContactToCompany(Long companyId, Long contactId) {
        Company company = companyRepository.findById(companyId).orElseThrow(() -> new ResourceNotFoundException(companyId));
        Contact contact = contactRepository.findById(contactId).orElseThrow(() -> new ResourceNotFoundException(contactId));
        company.addContact(contact);
        contact.addCompany(company);
        return companyRepository.save(company);
    }

    @Override
    public Company getByTVANumber(Integer tvaNumber) {
        return companyRepository.findByTvaNumber(tvaNumber).orElseThrow(
                () -> new ResourceNotFoundException(Long.valueOf(tvaNumber))
        );
    }

    @Override
    public Long delete(Long id) {
        Company company = companyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id));
        companyRepository.delete(company);
        return id;
    }

    @Override
    public List<Company> getCompanies() {
        return companyRepository.findAll();
    }
}
