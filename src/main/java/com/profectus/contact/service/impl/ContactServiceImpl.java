package com.profectus.contact.service.impl;

import com.profectus.contact.domain.Contact;
import com.profectus.contact.exception.ResourceNotFoundException;
import com.profectus.contact.repository.ContactRepository;
import com.profectus.contact.service.ContactService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ContactServiceImpl implements ContactService {

    private final ContactRepository repository;

    @Override
    public Contact create(Contact contact) {
        return repository.save(contact);
    }

    @Override
    public Contact update(Contact contactToUpdate) {
        if (contactToUpdate != null) {
            Contact contact = repository.findById(contactToUpdate.getId())
                    .orElseThrow(() -> new ResourceNotFoundException(contactToUpdate.getId()));
            contact.update(contactToUpdate);
            return repository.save(contact);
        }
        return null;
    }

    @Override
    public Long delete(Long id) {
        Contact contact = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id));
        repository.delete(contact);
        return id;
    }

    @Override
    public Contact getContact(Long id) {
        return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));
    }

    @Override
    public List<Contact> getContacts() {
        return repository.findAll();
    }
}
